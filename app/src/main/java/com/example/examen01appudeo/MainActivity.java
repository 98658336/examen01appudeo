package com.example.examen01appudeo;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private TextView tevewNombre;
    private Button btnEntrar;
    private Button btnCerrar;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tevewNombre = findViewById(R.id.tevewNombre);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnCerrar = findViewById(R.id.btnSalir);

        // Agregar el botón de cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // Evento del botón "Entrar"
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tevewNombre.getText().toString().isEmpty()) {
                    // Mostrar un mensaje de error al usuario
                    tevewNombre.setError("Ingrese un nombre");
                } else {
                    // Llamar a la actividad ReciboNominaActivity
                    Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
                    intent.putExtra("nombreTrabajador", tevewNombre.getText().toString());
                    startActivity(intent);
                }
            }
        });
    }
}