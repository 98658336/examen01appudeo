package com.example.examen01appudeo;

public class Recibo {

    private String nombreTrabajador;
    private String puesto;
    private int horasNormalesTrabajadas;
    private int horasExtrasTrabajadas;

    public Recibo(String nombreTrabajador, String puesto, int horasNormalesTrabajadas, int horasExtrasTrabajadas) {
        this.nombreTrabajador = nombreTrabajador;
        this.puesto = puesto;
        this.horasNormalesTrabajadas = horasNormalesTrabajadas;
        this.horasExtrasTrabajadas = horasExtrasTrabajadas;
    }

    public int calcularSubtotal() {
        // Calcula el subtotal
        int pagoPorHoraNormal = getPagoPorHora(puesto);
        int pagoPorHoraExtra = getPagoPorHoraExtra(puesto);

        return pagoPorHoraNormal * horasNormalesTrabajadas + pagoPorHoraExtra * horasExtrasTrabajadas;
    }

    public int calcularImpuesto() {
        // Calcula el impuesto
        return (int) (calcularSubtotal() * 0.16);
    }

    public int calcularTotalAPagar() {
        // Calcula el total a pagar
        return calcularSubtotal() - calcularImpuesto();
    }

    private int getPagoPorHora(String puesto) {
        // Devuelve el pago por hora según el puesto
        if (puesto.equals("Auxiliar")) {
            return 50;
        } else if (puesto.equals("Albañil")) {
            return 70;
        } else {
            return 100;
        }
    }

    private int getPagoPorHoraExtra(String puesto) {
        // Devuelve el pago por hora extra según el puesto
        if (puesto.equals("Auxiliar")) {
            return 100;
        } else if (puesto.equals("Albañil")) {
            return 140;
        } else {
            return 200;
        }
    }
}