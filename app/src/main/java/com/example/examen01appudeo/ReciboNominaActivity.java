package com.example.examen01appudeo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Spinner;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;


public class ReciboNominaActivity extends AppCompatActivity {


        private EditText txtRecibo;
        private EditText txtNombre;
        private EditText txtHorasNormales;
        private EditText txtHorasExtras;
        private Spinner spnPuesto;
        private TextView textViewSubtotal;
        private TextView textViewImpuesto;
        private TextView textViewResultado;
        private Button btnCalcular;
        private Button btnLimpiar;
        private Button btnSalir;

        @SuppressLint("MissingInflatedId")
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_recibo_nomina);

            txtRecibo = findViewById(R.id.txtrecibo);
            txtNombre = findViewById(R.id.txtNombre);
            txtHorasNormales = findViewById(R.id.txtHorasNormales);
            txtHorasExtras = findViewById(R.id.txtHorasExtras);
            spnPuesto = findViewById(R.id.spnPuesto);
            textViewSubtotal = findViewById(R.id.textViewSubtotal);
            textViewImpuesto = findViewById(R.id.textViewimpuesto);
            textViewResultado = findViewById(R.id.textViewResultado);
            btnCalcular = findViewById(R.id.btnCalcular);
            btnLimpiar = findViewById(R.id.btnLimpiar);
            btnSalir = findViewById(R.id.btnSalir);

            btnCalcular.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Calcular el subtotal
                    double subtotal = Double.parseDouble(txtHorasNormales.getText().toString()) * 100 + Double.parseDouble(txtHorasExtras.getText().toString()) * 150;

                    // Calcular el impuesto
                    double impuesto = subtotal * 0.16;

                    // Calcular el total a pagar
                    double total = subtotal + impuesto;

                    // Mostrar el resultado del cálculo
                    textViewSubtotal.setText(String.valueOf(subtotal));
                    textViewImpuesto.setText(String.valueOf(impuesto));
                    textViewResultado.setText(String.valueOf(total));
                }
            });

            btnLimpiar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Limpiar los campos de texto
                    txtRecibo.setText("");
                    txtNombre.setText("");
                    txtHorasNormales.setText("");
                    txtHorasExtras.setText("");
                    spnPuesto.setSelection(0);
                    textViewSubtotal.setText("");
                    textViewImpuesto.setText("");
                    textViewResultado.setText("");
                }
            });

            btnSalir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
}

